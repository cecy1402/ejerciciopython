import math


class nodeParish:
    # En esta clase definen las parroquias y sus atributos
    # Id: identificador de cada nodo.
    # Neighbors: lista de nodos para la conexión por rutas.
    # Visted: con esta variable se va a controlar si la parroquia ya fue visitada o no.
    # Distance: los km a recorrer.
    # Predecessor: Es el nodo que se visito un paso antes.

    def __init__(self, i):
        self.id = i
        self.distance = float('inf')
        self.neighbors = []
        self.visited = False
        self.predecessor = None

    def newNeighbor(self, n, d):
        #n: vecinos, d: distancia.
        # creación de la lista de nodos vecinos, con la condición de que este aun no este agregado en la lista de vecinos.
        if n not in self.neighbors:
            #estos datos se van almacenar en la lista de vecinos como otra lista.
            #En la posición 0(n), se encuentra el id del vecino y en (posición(1) DISTANCIA)
            self.neighbors.append([n, d])


class Graph:
    # Definición de los nodos de parroquias.
    def __init__(self):
        self.parish = {}

        
    def addParish(self, id):
		# Creación de Nodo(Parroquias), con la condición de que este aun no este
        # agregado en la lista de nodos y recibiendo como parametro el ID.
        if id not in self.parish:
            self.parish[id] = nodeParish(id)

        
    def addRoutes(self, a, b, d):
		# Creación de rutas, con la condición de que este aun no este agregado en la lista.
        # Recibiendo como parametro el id de dos nodos(a, b) y la distancia(d) entre ellos.
        # Y se asigna estos valores de los nodos, por medio del metodo newNeighbor().
        if a in self.parish and b in self.parish:
            #aqui vamos a obtener el valor del vecino y su peso.
            self.parish[a].newNeighbor(b, d)
            self.parish[b].newNeighbor(a, d)

        
    def road(self, a, b):
        #A: nodo inicial; B: nodo final
		# Aqui se almacenan los datos de los rutas recorridas, segun el orden que han sido visitadas.
        # Recibe dos parametros: el array de los rutas y la actual parroquia.
		#Se actualiza la lista de nodos, con los registros de menor distancia.
        road = []
        currentParish = b
        while currentParish != None:
            #se envia dos parametros para que se pueda guardar el registro al inicio de la lista.
            road.insert(0, currentParish)
            #actualizamos al nodo actual con el valor del predecesor.
            currentParish = self.parish[currentParish].predecessor
        return [road, self.parish[b].distance]

	
    def smaller(self, listNode):
        #listNode: Nodos no visitados
		# Se recibe la lista de los nodos no visitados si su valor es mayor a cero.
		# Al ser verdadera la condicion realiza comparaciones ente la distancia de los nodos para encontrar el valor menor.
        if len(listNode) > 0:
            # se toma el primer valor(distancia) que esta en la lista.
            dist = self.parish[listNode[0]].distance
            #Nodo que esta en la posicion 0 de la lista.
            v = listNode[0]
            #Aqui vamos a hacer una busqueda del nodo que tiene la menor distancia en nodos no visitados.
            for e in listNode:
                if dist > self.parish[e].distance:
                    #actualizamos la lista con la distancia
                    dist = self.parish[e].distance
                    #Actualizamos el nodo con la nueva distancia.
                    v = e
            return v
        return None

    def calculateShortRoute(self, a):
        # obtención de la ruta mas corta aplicando el algoritmo de Dijkstra
        #Verificamos que el valor "a" se encuentre en la lista de nodos.
        if a in self.parish:
            #PASO 1: Asignación al nodo inicial una distancia tentativa:0 e infinito para los nodos restantes.
            self.parish[a].distance = 0
			#PASO: 2 se toma como nodo actual (A)
            currentParish = a 
			# se declara un array de nodos no visitados.
            noVisited = []
            #node: es una variable para identificar los "nodos".
            for node in self.parish:
                if node != a:
                    #vamos a establecer la distancia del valor del nodo como infinito.
                    self.parish[node].distance = float('inf')
                self.parish[node].predecessor = None
                #se agrega en no visitados a los vertices con predecesor nulo.
                noVisited.append(node)

            while len(noVisited) > 0:
			#PASO 3: En el nodo actual se conseideran todos los nodos vecinos no visitados con su distancia.
                for neig in self.parish[currentParish].neighbors:
                    #Aqui visitamos el id de vecinos y validamos que no este aun visitada.
                    if self.parish[neig[0]].visited == False:
                        #Si la suma de distancia del nodo actual + su peso, es < a la distancia que su vecino tiene almacenada, hacemos el intercambio por el #menor.
                        if self.parish[currentParish].distance + neig[1] < self.parish[neig[0]].distance:
                            #Actualizacion de la distancia del nodo actual.
                            #La distancia del nodo vecino sera igual a la distancia del nodo actual + la distancia del vecino. 
                            self.parish[neig[0]].distance = self.parish[currentParish].distance + neig[1]
                            #Aqui asignamos como predecesor al nodo actual, del nodo vecino.
                            self.parish[neig[0]].predecessor = currentParish

                #PASO 5: una vez revisado todos los vecinos del nodo actual, se marca como nodo visitado y se lo elimina de los nodos no visitados.
                self.parish[currentParish].visited = True
                noVisited.remove(currentParish)

				#PASO 6: Se toma como próximo nodo actual el de menor valor en D (puede hacerse almacenando los valores en una cola de prioridad) y se regresa al paso 3,
				#mientras existan nodos no marcados.
                currentParish = self.smaller(noVisited)
        else:
            return False


class main:
    # id. de las parroquias.
    g = Graph()
    g.addParish(1)
    g.addParish(2)
    g.addParish(3)
    g.addParish(4)
    g.addParish(5)
    g.addParish(6)
    g.addParish(7)
    g.addParish(8)

    # aqui se declaran las rutas alternas y la ruta vecina de conexion y la distancia(km).
    g.addRoutes(1, 2, 7.5)
    g.addRoutes(1, 3, 6)
    g.addRoutes(2, 5, 11)
    g.addRoutes(3, 4, 15)
    g.addRoutes(3, 6, 7)
    g.addRoutes(4, 8, 18)
    g.addRoutes(4, 7, 23)
    g.addRoutes(5, 7, 20)
    g.addRoutes(6, 8, 25.5)
    g.addRoutes(7, 8, 12.5)

    # presentacion del resultado por consola
    print("\nLa ruta más rápida para llegar a su destino es:")
    g.calculateShortRoute(1)  # ruta actual
    print(g.road(1, 8))  # ruta de inicio y de fin.
